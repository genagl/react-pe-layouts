
import Layouts from "./layouts"

export function modules() {
  return Layouts().modules
    ? Layouts().modules
    : {

    }
}
export function moduleExists( moduleName )
{
  return !!modules()[ moduleName ]
}
