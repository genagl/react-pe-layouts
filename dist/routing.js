function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from "react";
import { Link } from "react-router-dom";
import Layouts from "./layouts";
export function concatRouting() {
  let routing = [];

  for (const key in Layouts().routing) {
    routing = routing.concat(get(key));
  }

  return routing;
}
export function getAllRoutes() {
  const routes = [];
  concatRouting().forEach(route => {
    if (route.children) {
      const e1 = { ...route
      };
      delete e1.children;
      routes.push(e1);
      route.children.forEach(childRoute => {
        const ee1 = { ...childRoute
        };

        if (ee1.children) {
          delete ee1.children;
          routes.push(ee1); //childRoute.children.forEach((eee) => {

          childRoute.children.forEach(gChildRoute => {
            const eee1 = { ...gChildRoute
            };

            if (eee1.children) {
              delete ee1.children;
              routes.push(eee1);

              if (gChildRoute.children) {
                gChildRoute.children.forEach(ggChildRoute => {
                  if (ggChildRoute.children) {
                    ggChildRoute.children.forEach(subChildRoute => {
                      const eeeee1 = { ...subChildRoute
                      };

                      if (eeeee1.children) {
                        delete eeeee1.children;
                        routes.push(eeeee1);
                        subChildRoute.children.forEach(eeeeee => {
                          routes.push(eeeeee);
                        });
                      } else {
                        routes.push(eeeee1);
                      }
                    });
                  }
                });
              }
            } else {
              routes.push(eee1);
            }
          }); //})
        } else {
          routes.push(ee1);
        }
      });
    } else {
      routes.push(route);
    }
  });
  return routes;
}
export function getSingleRoute(data_type) {
  //console.log(data_type, getAllRoutes())
  return getAllRoutes().filter(e => e.singled_data_type === data_type)[0];
}
export const getFeedRoute = data_type => {
  return getAllRoutes().filter(e => e.feed_data_type === data_type)[0];
};
export function getAdminRouteLink(data_type, content) {
  // console.log( getAllRoutes( ) );
  const dt = getAllRoutes().filter(e => e.data_type == data_type); // console.log( data_type, dt );

  if (dt[0]) {
    return /*#__PURE__*/React.createElement(Link, {
      to: dt[0].route
    }, content);
  }

  return content;
}
export function getByRoute(route) {
  const rountArray = route.split("/");

  if (rountArray[0] === "") {
    rountArray.splice(0, 1);
  }

  const components = {};

  function importAll(r) {
    r.keys().forEach(key => {
      const key1 = key.replace("./", "").split(".").slice(0, -1).join(".");
      components[key1] = r(key);
    });
  } //importAll(require.context("../states/", false, /\.js$/))


  function getContent(route) {
    // return route.title;
    if (route.component) {
      const Component = components[route.component].default;
      return /*#__PURE__*/React.createElement(Component, _extends({}, route, {
        onChangeStyle: style => null
      }));
    }

    if (route.html_source) {
      const HTMLSourceState = components.HTMLSourceState.default;
      return /*#__PURE__*/React.createElement(HTMLSourceState, _extends({}, route, {
        onChangeStyle: style => null
      }));
    }

    if (route.html) {
      const HTMLState = components.HTMLState.default;
      return /*#__PURE__*/React.createElement(HTMLState, _extends({}, route, {
        onChangeStyle: style => null
      }));
    }
  }

  const ret = concatRouting().map(e => {
    if (e.route === rountArray[0]) {
      if (rountArray.length === 1) {
        console.log(e);
        return getContent(e);
      }

      if (e.route === rountArray[1]) {
        return null;
      } else {
        return null;
      }
    }

    return null;
  });
  return ret;
}
export function routeData(e = undefined, child = undefined, grandchild = undefined, forceRoute = undefined) {
  let preroute;
  let route;
  let routeBasic;
  let noexact_route;
  let currentE;
  let capability;

  if (grandchild) {
    // console.log("grandchild");
    // console.log(grandchild);
    capability = grandchild.capability;
    preroute = `/${e.route}/${child.route}`;
    route = typeof forceRoute !== "undefined" ? forceRoute : `${grandchild.route}`;
    noexact_route = typeof forceRoute !== "undefined" ? forceRoute : `${grandchild.route}/:id`;
    currentE = grandchild;
  } else if (child) {
    // console.log("child");
    // console.log(child);
    capability = child.capability;
    preroute = `/${e.route}`;
    route = typeof forceRoute !== "undefined" ? forceRoute : child.route;
    noexact_route = forceRoute || `${child.route}/:id`;
    currentE = child;
  } else {
    // console.log("e");
    // console.log(e);
    capability = e.capability;
    preroute = "";
    route = typeof forceRoute !== "undefined" ? forceRoute : e.route;
    noexact_route = forceRoute || `${e.route}/:id`;
    currentE = e;
  }

  return {
    currentE,
    preroute,
    route,
    noexact_route,
    capability
  };
}
export function existRouting(key = "") {
  const routingArray = Layouts().routing[key];
  return routingArray && routingArray.length > 0;
}
export function existRoutingChilder(key = "") {
  const routingArray = Layouts().routing[key][0];
  return routingArray.children && routingArray.children.length > 0;
}
export function getFirstRoute(key = "") {
  return Layouts().routing[key][0];
}
export function mainPage() {
  let main = Layouts().routing.extended_routes.filter(e => e.route === "")[0];

  if (!main) {
    main = Layouts().routing.menu[0];
  }

  return main;
}
export function mainMenu() {
  return exec_route(Layouts().routing.main_menu);
}
export function menu() {
  return exec_route(Layouts().routing.menu);
}
export function footer() {
  return exec_route(Layouts().routing.footer);
}
export function profile() {
  return exec_route(Layouts().routing.profile);
}
export function get(key = "") {
  const dd = Layouts().routing[key]; // console.log(dd, key);

  return dd;
}
export function routing() {
  return Layouts().routing;
}
export function right_routing() {
  const r = {};

  for (const route in Layouts().routing) {
    console.log(route);
    r[route] = exec_route(Layouts().routing[route]);
  }

  return r;
}
export function link() {
  return exec_route(Layouts().routing.link);
}
export function exec_route(route_array) {
  if (!route_array) return;
  const routing = concatRouting();
  const rArray = [];
  route_array.forEach((e, i) => {
    if (typeof e.target_id !== "undefined") {
      routing.forEach((ee, ii) => {
        if (ee.route == e.target_id) {
          rArray.push({ ...ee,
            title: e.title,
            icon: e.icon
          });
        }
      });
    } else rArray.push(e);
  });
  return rArray;
}
export function default_menu() {
  return {
    profile: {
      depth: 1,
      description: "User avatar's submenu",
      icon: "profile-menu-icon.svg",
      title: "Current User profile"
    },
    extended_routes: {
      depth: 1,
      description: "Routes outside all menu groups",
      icon: "extend-menu-icon.svg",
      title: "Outside routes"
    },
    main_menu: {
      depth: 5,
      description: "Main menu group in header",
      icon: "header-menu-icon.svg",
      title: "Header Menu"
    },
    menu: {
      depth: 3,
      description: "Menu group of left of page Content (may be hidden)",
      icon: "left-menu-icon.svg",
      title: "Left Menu"
    },
    footer: {
      depth: 1,
      description: "Footer menu group",
      icon: "footer-menu-icon.svg",
      title: "Footer Menu"
    }
  };
}