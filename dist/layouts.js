/*
 *  PE Application layout configs
*/
let layout = {};
let routing = {};
let app = {};
let widgetAreas = {};
let template = {};
export const layoutInit = layoutSource => {
  layout = { ...layoutSource
  };
  return true;
};

const Layouts = () => {
  if (!layout.app || !layout.template || !layout.routing || !layout.modules || !layout.schema) {
    throw new Error("PE Application need layout config file. See more: ");
  }

  let l = { ...layout
  };

  if (Object.keys(routing).length > 0) {
    //console.log(routing)
    l.routing = routing.main_menu ? routing : l.routing;
    l.app = app.title ? app : l.app;
    l.template = template.styles ? template : l.template;
    l['widget-area'] = widgetAreas;
  }

  return l;
};

export { initConfig, google_analytics__, app_url, gitlab_private_token, link_type__, server_url__, sourse_url__, upload_url__, assertion_token__, yandex_map_api_key__, app_layouts__ } from "config";
export { client } from "./client";
export { yandex_map_api_key, geoDefaultPosition, geoPosition, zoom } from "./map";
export * from "./app";
export * from "./menu";
export * from "./modules";
export * from "./routing";
export * from "./template";
export * from "./schema/index";
export default Layouts;
export const updateRoutes = menu => {
  if (typeof menu === "object") {
    //console.log(layout.routing) 
    //console.log(menu) 
    routing = { ...layout.routing,
      ...menu
    };
  }
};
export const updateApp = _app => {
  if (typeof _app === "object") {
    //console.log(_app) 
    app = { ...layout.app,
      ..._app
    };
  }
};
export const updateWidgets = _widgets => {
  //console.log(_widgets) 
  if (typeof _widgets === "object") {
    widgetAreas = { ...layout['widget-area'],
      ..._widgets
    };
  }
};
export const updateTemplate = _template => {
  //console.log(_template) 
  if (typeof _template === "object") {
    template = { ...layout.template,
      ..._template
    };
  }
};